LineCounter

Cuenta lineas negras y muestra el número de lineas contadas en el display LCD

Switch1 --> prende y apaga el display y detiene el conteo. Al reiniciar el conteo,la cuenta vuelve a cero. 

Switch2--> Hold. Detiene el conteo. Al volver apretar hold la cuenta continua desde el valor que tenia .

Switch3--> Reset. Vuelve el conteo a cero. 

CONEXIONES

		       	   	||  Periferico  	|| 		EDU-CIAA-NXP

Tcrt5000

 				   	||	DOUT			||	     T_FIL2
 				   	||  +5V				||		 +5V
 				   	||	GND				||		 GND

DISPLAY ITSE0803
					||	D1				||		LCD1
					||	D2				||		LCD2
					||	D3				||		LCD3
					||	D4				||		LCD4
					||	SEL_0			||		GPIO1
					||	SEL_1			||		GPIO3
					||	SEL_2			||		GPIO5
					||	+5V				||		+5V
					||	GND				||		GND

