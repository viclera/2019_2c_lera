


/*==================[inclusions]=============================================*/
#include "../inc/LineCounter.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "delay.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "bool.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
bool estado=true; //estado==1 -->ON		estado==0-->OFF
bool hold=false;
uint8_t LineCount=0;

/*==================[internal functions declaration]=========================*/
void OnOff (void)
{
			if(estado==false)
				estado=true;
			else
			{
				estado=false;
				LineCount=0;
			}
}
void Hold(void)
{
			if(hold==false)
					hold=true;
			else
					hold=false;
}
void Reset(void)
{
	LineCount=0;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializacion de los pines del del display LCD
	gpio_t PINES[7];
		PINES[0]=GPIO5;
		PINES[1]=GPIO3;
		PINES[2]=GPIO1;
		PINES[3]=LCD1;
		PINES[4]=LCD2;
		PINES[5]=LCD3;
		PINES[6]=LCD4;

	//Inicializacion del display LCD
	ITSE0803Init(PINES);

	//Inicializacion de los switches
	SwitchesInit();

	//Inicializacion del
	gpio_t x;
	x=T_FIL2;
	Tcrt5000Init(x);

	//Inicializacion del clock
	SystemClockInit();




	bool PreviousState, ActualState;
	PreviousState=ActualState=Tcrt5000State();

	//Definicion de las interrupciones de los Switches
	SwitchActivInt(SWITCH_1,OnOff);
	SwitchActivInt(SWITCH_2,Hold);
	SwitchActivInt(SWITCH_3,Reset);



	while(1){

		if (estado==true) /*controla estado display y contador*/
		{
			if (hold==false) /*actualiza  conteo*/
			{
					ActualState=Tcrt5000State();
					if(ActualState!=PreviousState)
					{
						if(ActualState==0)
						{
							LineCount++;}
						}
					PreviousState=ActualState;
					/*DelayMs(10);*/
			}

		ITSE0803DisplayValue(LineCount); /*muestra valor*/
		}

		if(estado==false)
		{
			ITSE0803Blank();
		}

	}
	return 0;


}
/*==================[end of file]============================================*/

