Musculometro AKA Biofeedback

Prende una cantidad de leds en el NeoPixel proporcional a la amplitud del EMG 
Envia los valores de la señal de EMG por BT


CONEXIONES

		       	   	||  Periferico  	|| 		EDU-CIAA-NXP

NEOPIXEL
					||	+5V				||		+5V
					||	GND				||		GND
					||	DATA INPUT		||		GPIO5
					
AMPLIF. BIOPOTENCIALES
					||	+3.3V			||		+3.3V
					||	GND				||		GND
					||	OUT				||		CH1

BRIDGE BT
					||	STATE			||			-
		       	   	||	RXD				||		232_TX
		       	   	||	TXD				||		232_RX
		       	   	||	GND				||		GND
		       	   	||	VCC				||		+3.3V
		       	   	||	EN				||		-