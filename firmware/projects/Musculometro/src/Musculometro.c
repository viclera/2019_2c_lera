/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "analog_io.h"
#include "NeoPixel.h"
#include "timer.h"
#include "delay.h"
#include "systemclock.h"
#include <math.h>
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

uint16_t valor_senial;


/*==================[internal functions declaration]=========================*/

void Send(void)
{
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*G");

	UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(valor_senial,10));

	UartSendString(SERIAL_PORT_P2_CONNECTOR,"*");


}

void TurnOnLEDS(void)
{
	uint8_t num_leds;
	num_leds=((abs(valor_senial-500)*12)/(512))*2;
	NeoPixelSetLen(num_leds);
	NeoPixelAllBlue(200);


}

void Read(void)
{
	 AnalogInputRead(CH1,&valor_senial);
	 TurnOnLEDS();
	 Send();

}

void Emp_conver(void)
{
	NeoPixelAllOFF();
	AnalogStartConvertion();
}

void IntRecep(void)
{

}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializa clock
	SystemClockInit();

	//Inicializa Neo pixel  de 12 LEDs
	NeoPixelInit(5,12);

	//Inicializa conversor AD
	analog_input_config  AnalogInputConfig={CH1,AINPUTS_SINGLE_READ,Read};
	analog_input_config *ptr_AnalogInputConfig=&AnalogInputConfig;
	AnalogInputInit(ptr_AnalogInputConfig);

	//Inicializa la uart
	serial_config configUART={SERIAL_PORT_P2_CONNECTOR,9600,IntRecep};
	serial_config *ptr_configUART=&configUART;
	UartInit(ptr_configUART);

	//Inicializa timer, cada 300ms salta una interrupcion que inicia la conversion
	timer_config config={TIMER_B,30,Emp_conver};
	timer_config *ptr_config;
	ptr_config=&config;
	TimerInit(ptr_config);
	TimerStart(TIMER_B);

while(1){



}

    
	return 0;
}

/*==================[end of file]============================================*/

