


/*==================[inclusions]=============================================*/
#include "../inc/Guia3_Ejercitacion.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "delay.h"
#include "systemclock.h"
#include "Tcrt5000.h"
#include "switch.h"
#include "bool.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
bool estado=true; //estado==1 -->ON		estado==0-->OFF
bool hold=false;
uint16_t Tiempo=0;

/*==================[internal functions declaration]=========================*/
void OnOff (void)
{
			if(estado==false)
				estado=true;
			else
			{
				estado=false;
				LineCount=0;
			}
}
void Hold(void)
{
			if(hold==false)
					hold=true;
			else
					hold=false;
}
void Reset(void)
{
	Tiempo=0;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializacion de los pines a utilizar
	gpio_t PINES[7];
		PINES[0]=GPIO5;
		PINES[1]=GPIO3;
		PINES[2]=GPIO1;
		PINES[3]=LCD1;
		PINES[4]=LCD2;
		PINES[5]=LCD3;
		PINES[6]=LCD4;

	// Inicializacion del display
	ITSE0803Init(PINES);

	//Inicializacion de los switches
	SwitchesInit();
	// Inicializacion del clock
	SystemClockInit();

	// Definicion de las interrupciones correspondientes a cada switch
	SwitchActivInt(SWITCH_1,Cent);
	SwitchActivInt(SWITCH_2,OnOff);
	SwitchActivInt(SWITCH_3,Reset);
	SwitchActivInt(SWITCH_4,Hold);



	while(1){

		if (estado==true) /*controla estado display y contador*/
		{
			if (hold==false) /*actualiza  conteo*/
			{

			}

		ITSE0803DisplayValue(Tiempo); /*muestra valor*/
		}

		if(estado==false)
		{
			ITSE0803Blank();
		}

	}
	return 0;


}
/*==================[end of file]============================================*/

