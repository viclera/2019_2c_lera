


/*==================[inclusions]=============================================*/
#include "../inc/TEST_DisplayITS0803.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "delay.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000
/*==================[internal data definition]===============================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	SystemClockInit();

	gpio_t PINES[7];
	PINES[0]=GPIO5;
	PINES[1]=GPIO3;
	PINES[2]=GPIO1;
	PINES[3]=LCD1;
	PINES[4]=LCD2;
	PINES[5]=LCD3;
	PINES[6]=LCD4;


	ITSE0803Init(PINES);

	uint16_t b=0;
	while(1){

		ITSE0803DisplayValue(b);
		DelayMs(100);
		b++;
	}
    
	return 0;
}

/*==================[end of file]============================================*/

