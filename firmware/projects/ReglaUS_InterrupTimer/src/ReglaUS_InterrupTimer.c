/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "ReglaUS_InterrupTimer.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"
#include "timer.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

bool onoff=true;
bool hold=false;
bool cm=true;
bool pg=false;
uint16_t dist;

/*==================[internal functions declaration]=========================*/
void Medir(void)
{
	if(onoff==true)
	{	if (hold==false)
		{
			if(cm==true)
			{
				dist=HcSr04ReadDistanceCentimeters();
			}

			if(pg==true)
			{
				dist=HcSr04ReadDistanceInches();
			}
		}

	ITSE0803DisplayValue(dist);
	}
	else
		ITSE0803Blank();

}


void OnOff (void)
{
			if(onoff==false)
				onoff=true;
			else
			{
				onoff=false;

			}
}
void Hold(void)
{
			if(hold==false)
					hold=true;
			else
					hold=false;
}

void Cm(void)
{
	cm=true;
	pg=false;
}

void Pg(void)
{
	pg=true;
	cm=false;

}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializa medidor US
	HcSr04Init(T_FIL2,T_FIL3);

	//Inicializa clock
	SystemClockInit();

	//Inicializa pines del display LCD
	gpio_t PINES[7];
			PINES[0]=GPIO5;
			PINES[1]=GPIO3;
			PINES[2]=GPIO1;
			PINES[3]=LCD1;
			PINES[4]=LCD2;
			PINES[5]=LCD3;
			PINES[6]=LCD4;

	// Inicializa Display LCD
	ITSE0803Init(PINES);

	//Inicializa switches
	SwitchesInit();
	// Define funciones de interrupcion correspondientes a cada switch
	SwitchActivInt(SWITCH_1,OnOff);
	SwitchActivInt(SWITCH_2,Hold);
	SwitchActivInt(SWITCH_3,Cm);
	SwitchActivInt(SWITCH_4,Pg);

	//Inicializa timer y define su funcion de interrupcion
	timer_config config={TIMER_B,1000,Medir};
	timer_config *ptr_config;
	ptr_config=&config;
	TimerInit(ptr_config);
	TimerStart(TIMER_B);
while(1){}

return 0;
    
}

/*==================[end of file]============================================*/

