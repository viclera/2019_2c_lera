ReglaUS_InterrupTimer

Mide distancia cada 1 s y la muestra en el display LCD. 

SWITCH1--> ON/OFF 

SWITCH2--> HOLD. Mantiene el valor de la medicion 

SWITCH3--> Muestra la distancia en cm

SWITCH4--> Muestra la distancia en pulgadas



CONEXIONES

		       	   	||  Periferico  	|| 		EDU-CIAA-NXP

HcSr04

 				   	||	ECHO			||	     T_FIL2
 				   	||	TRIGGER			||		 T_FIL3
 				   	||  +5V				||		 +5V
 				   	||	GND				||		 GND


DISPLAY ITSE0803
					||	D1				||		LCD1
					||	D2				||		LCD2
					||	D3				||		LCD3
					||	D4				||		LCD4
					||	SEL_0			||		GPIO1
					||	SEL_1			||		GPIO3
					||	SEL_2			||		GPIO5
					||	+5V				||		+5V
					||	GND				||		GND