PARCIAL PROGRAMABLE DEL 28/10/2019

La aplicacion toma una señal de presion que varia en 0 y 200mmHg a una frecuencia de 250Hz. 
Cada 1 seg, calcula maximo, minimo y promedio de la señal en el ultimo segundo. 
Luego, segun la tecla elegida (Switch1=maximo, Switch2=minimo, Switch3=promedio) muestra el valor en el display LCD 
y lo envia a la PC por la UART
A su vez, enciende un led que indica el valor que se esta mostrando LED1=maximo, LED2=minimo, LED3=promedio

Tambien, de acuerdo al valor del maximo enciende el Led ROJO del RGB (si el maximo es mayor a 150), el LED AZUL del RGB
(si el maximo se encuentra entre 150 y 50) y el LED VERDE ( si el maximo es menor a 50)


CONEXIONES

		       	   	||  Periferico  	|| 		EDU-CIAA-NXP

DISPLAY ITSE0803
					||	D1				||		LCD1
					||	D2				||		LCD2
					||	D3				||		LCD3
					||	D4				||		LCD4
					||	SEL_0			||		GPIO1
					||	SEL_1			||		GPIO3
					||	SEL_2			||		GPIO5
					||	+5V				||		+5V
					||	GND				||		GND

Sensor Presion
					||	+3.3V			||		+3.3V
					||	GND				||		GND
					||	OUT				||		CH1
					
