/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "analog_io.h"
#include "timer.h"

#include  "led.h"
#include "uart.h"
#include  "DisplayITS_E0803.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

#define Frec_muestreo 250

/*==================[internal data definition]===============================*/

uint16_t valor_senial;
uint16_t valor_presion[Frec_muestreo];

uint16_t maximo = 0;
uint16_t minimo = 200;
uint16_t promedio = 0;
uint16_t num_lecturas=0;
uint16_t valor_a_mostrar;

/*==================[internal functions declaration]=========================*/

//Funcion para buscar el maximo de la señal en el ultimo segundo. No recibe ni retorna parametros
void max()
{
	uint16_t i;
		for (i=0;i<Frec_muestreo;i++)
		{
			if (valor_presion[i]>maximo)
			maximo=valor_presion[i];
		}
}

//Funcion para buscar el minimo de la señal en el ultimo segundo. No recibe ni retorna parametros
void min(void)
{
	uint16_t i;
	for (i=0;i<Frec_muestreo;i++)
	{
		if (valor_presion[i]<minimo)
		minimo=valor_presion[i];
	}
}

//Funcion para buscar el promedio de la señal en el ultimo segundo. No recibe ni retorna parametros
void prom(void)
{
	uint16_t i;
		for (i=0;i<Frec_muestreo;i++)
		{
			promedio=promedio+valor_presion[i];
		}
		promedio=promedio/Frec_muestreo;
}


void check_max(void)
{
	if (maximo>150)
	{
			LedOn(LED_RGB_R);
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);
	}
	if (maximo<150 && maximo>50)
	{
			LedOn(LED_RGB_B);
			LedOff(LED_RGB_G);
			LedOff(LED_RGB_R);
	}
	if (maximo<50)
	{
			LedOn(LED_RGB_G);
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_R);
	}

}
//Funcion para llenar un vector que contiene los valores de la señal (en presion, es decir mmHg)
//en el ultimo segundo (250 muestras en un segundo)
// Recibe como parametro el valor de la señal. No retorna ningun valor.
void llenar_vector(uint16_t val)
{
	valor_presion[num_lecturas]=(val*200)/1023;

}

// Funcion de interrupcion del switch 1.
void  show_max(void)
{
	valor_a_mostrar=maximo;
	LedOff(LED_2);
	LedOff(LED_3);
	LedOn(LED_1);

}
// Funcion de interrupcion del switch 2
void  show_min(void)
{
	valor_a_mostrar=minimo;
	LedOff(LED_3);
	LedOff(LED_1);
	LedOn(LED_2);
}
// Funcion de interrupcion del switch 3
void  show_prom(void)
{
	valor_a_mostrar=promedio;
	LedsOffAll();
	LedOn(LED_3);
	LedOff(LED_1);
	LedOff(LED_2);
}

//Funcion de interrupcion de lectura del conversor AD
void Read(void)
{
	 AnalogInputRead(CH1,&valor_senial);
	 llenar_vector(valor_senial);
	 num_lecturas++ ;

}

//Funcion de interrupcion del timer. Comienza la conversion
void Emp_conver(void)
{
	AnalogStartConvertion();
}


void IntRecep(void)
{

}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializa clock
	SystemClockInit();

	//Inicializa conversor AD
	analog_input_config  AnalogInputConfig={CH1,AINPUTS_SINGLE_READ,Read};
	analog_input_config *ptr_AnalogInputConfig=&AnalogInputConfig;
	AnalogInputInit(ptr_AnalogInputConfig);

	//Inicializa la uart
	serial_config configUART={SERIAL_PORT_PC,9600,IntRecep};
	serial_config *ptr_configUART=&configUART;
	UartInit(ptr_configUART);

	//Inicializa timer, cada 4ms(250Hz) salta una interrupcion que inicia la conversion
	timer_config config={TIMER_B,(1000/Frec_muestreo),Emp_conver};
	timer_config *ptr_config;
	ptr_config=&config;
	TimerInit(ptr_config);
	TimerStart(TIMER_B);


	//Inicializa switches
	SwitchesInit();
	// Define funciones de interrupcion correspondientes a cada switch
	SwitchActivInt(SWITCH_1,show_max);
	SwitchActivInt(SWITCH_2,show_min);
	SwitchActivInt(SWITCH_3,show_prom);

	//Inicializa pines del display LCD
		gpio_t PINES[7];
				PINES[0]=GPIO5;
				PINES[1]=GPIO3;
				PINES[2]=GPIO1;
				PINES[3]=LCD1;
				PINES[4]=LCD2;
				PINES[5]=LCD3;
				PINES[6]=LCD4;

	// Inicializa Display LCD
	ITSE0803Init(PINES);


	//Inicializa LEDs
	LedsInit();

while(1){

	if(num_lecturas==Frec_muestreo)
		 {
			 max();
			 min();
			 prom();
			 ITSE0803DisplayValue(valor_a_mostrar);
			 UartSendString(SERIAL_PORT_PC,UartItoa(valor_a_mostrar,10));
			 check_max();
			 num_lecturas=0;
			 promedio=0;
			 maximo=0;
			 minimo=200;
		 }

}

    
	return 0;
}

/*==================[end of file]============================================*/

