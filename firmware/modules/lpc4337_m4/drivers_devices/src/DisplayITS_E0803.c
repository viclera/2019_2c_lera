/*
 * DisplayITS_E0803.c
 *
 *  Created on: 6 sep. 2019
 *      Author: Victoria Lera
 */

/*==================[inclusions]=============================================*/

#include "DisplayITS_E0803.h"

#include "delay.h"

/*==================[internal data definition]===============================*/

#define DigitosDisplay  3
#define BitsBCD 4
#define NUMEROPINES 7

uint16_t VALUE;

/* MyPins [SEL0, SEL1, SEL2,MSB, , ,LSB]*/
gpio_t MyPins[NUMEROPINES];
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool ITSE0803Init(gpio_t * pins)
{
	uint8_t i;
	for (i=0;i<NUMEROPINES;i++)
	{
	GPIOInit(pins[i], GPIO_OUTPUT);
	GPIOOff(pins[i]);
	MyPins[i]=pins[i];
	}
	return true;
}

bool ITSE0803DisplayValue(uint16_t valor)
{

	VALUE = valor;
	uint8_t SELxBitsBCD[DigitosDisplay][BitsBCD];
	uint8_t i;
	uint8_t j;
	uint8_t aux;
	for(i=0;i<DigitosDisplay;i++)
	{
	    	aux=valor%10; // en aux queda guardada la unidad de valor
	    	valor = valor/10; // valor pierde su ultimo digito
	    	for(j=0;j<BitsBCD;j++)
	    	{
	    		SELxBitsBCD[i][j]=aux%2; // en aux2 queda el resto de la division por dos
	    		aux=aux/2;

	    	}
	}

	for(i=0;i<DigitosDisplay;i++)
	{

		for(j=0;j<BitsBCD;j++)
		{
		if (SELxBitsBCD[i][j]==1)
			GPIOOn(MyPins[DigitosDisplay+j]);
		else
			GPIOOff(MyPins[DigitosDisplay+j]);
		}

		GPIOOn(MyPins[i]);

		GPIOOff(MyPins[i]);

	}

	return true;
}


uint16_t ITSE0803ReadValue(){

	return VALUE;
}




bool ITSE0803InitDef()
{
/** Configuration of the GPIO */
		GPIOInit(LCD1, GPIO_OUTPUT);
		GPIOInit(LCD2, GPIO_OUTPUT);
		GPIOInit(LCD3, GPIO_OUTPUT);
		GPIOInit(LCD4, GPIO_OUTPUT);
		GPIOInit(GPIO1, GPIO_OUTPUT);
		GPIOInit(GPIO3, GPIO_OUTPUT);
		GPIOInit(GPIO5, GPIO_OUTPUT);


		/** Turn off pins*/
			GPIOOff(LCD1);
			GPIOOff(LCD2);
			GPIOOff(LCD3);
			GPIOOff(LCD4);
			GPIOOff(GPIO1);
			GPIOOff(GPIO3);
			GPIOOff(GPIO5);


			return true;
}


bool ITSE0803Blank()
{
	uint8_t i;
	for(i=0;i<BitsBCD;i++){
		GPIOOn(MyPins[DigitosDisplay+i]); /*Pone en 1 los 4 bits del BCD*/
		}
	GPIOOn(MyPins[0]);
	GPIOOn(MyPins[1]);
	GPIOOn(MyPins[2]);

	return true;
}
