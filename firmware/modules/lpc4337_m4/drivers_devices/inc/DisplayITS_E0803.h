/*
 * DisplayITS_E0803.h
 *
 *  Created on: 6 sep. 2019
 *      Author: victo
 */

#ifndef DISPLAYITS_E0803_H
#define DISPLAYITS_E0803_H


/*==================[inclusions]=============================================*/


#include "bool.h"
#include <stdint.h>
#include "gpio.h"
/*==================[external functions declaration]=========================*/

/** @brief Initialization function of EDU-CIAA pins
 *
 * @param[in] pins
 *
 * @return TRUE if no error
 */
bool ITSE0803Init(gpio_t * pins);


/** @brief Function to display value
 *
 * @param[in] Value
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */

bool ITSE0803DisplayValue(uint16_t valor);


/** @brief Function to read the value
 *
 * @param[in] No parameter
 *
 * @return value
 */

uint16_t ITSE0803ReadValue(void);


/** @brief Deinitialization function of EDU-CIAA pins
 *
 * @param[in] pins
 *
 * @return TRUE if no error
 */

bool ITSE0803Deinit(gpio_t * pins);




/** @brief Initialization function of EDU-CIAA pins by default
 *
 * @param[in] No  Parameter
 *
 * @return TRUE if no error
 */
bool ITSE0803InitDef(void);



/** @brief Turn off display
 *
 * @param[in] No  Parameter
 *
 * @return TRUE if no error
 * */
bool ITSE0803Blank(void);

#endif
