/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../4_guia1_INTEG_A/inc/main.h"

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

typedef struct
{
    uint8_t port;                /*!< GPIO port number */
    uint8_t pin;                /*!< GPIO pin number */
    uint8_t dir;                /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


void BCDPort(uint8_t digito, gpioConf_t * gpios){

uint8_t BitsBCD[4];
uint8_t j,k;
for(j=0;j<4;j++){
    		BitsBCD[j]=digito%2; // El primer elemento de BCD es el LSB
    		digito=digito/2;
}
for(k=0;k<4;k++){
printf(" en el puerto %d.%d hay un %d \n", gpios[k].port,gpios[k].pin,BitsBCD[k]);
}


}



int main(void)
{
uint8_t a=7;

	gpioConf_t vec_puertos[4];
	vec_puertos[0].port=1;
	vec_puertos[0].pin=4;

	vec_puertos[1].port=1;
	vec_puertos[1].pin=5;

	vec_puertos[2].port=1;
	vec_puertos[2].pin=6;

	vec_puertos[3].port=2;
	vec_puertos[3].pin=14;

	BCDPort(a,vec_puertos);

	return 0;
}

/*==================[end of file]============================================*/

