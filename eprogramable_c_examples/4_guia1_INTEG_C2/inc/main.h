
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/
int main(void);

typedef struct
	{
	    uint8_t n_led;       // indica el número de led a controlar
	    uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
	    uint8_t periodo;    //indica el tiempo de cada ciclo
	    uint8_t MODE;      // ON, OFF, TOGGLE
	}leds;

void Control_LEDS(leds aux);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

